Snapdragon Initial Setup

Helpful Links:
* https://docs.px4.io/en/flight_controller/snapdragon_flight_dev_environment_installation.html
* https://docs.px4.io/en/flight_controller/snapdragon_flight_advanced.html 
* https://github.com/ATLFlight/qflight_descriptions
* https://github.com/ATLFlight/QFlightProDocs/blob/master/RosSoftware.md 

First, plug a USB-micro into the side of the compute board and connect your computer to it.
Add your user to the dialout group with:
`sudo usermod -a -G dialout $USER`
Note: This requires you to logout and back in to take effect.

Then install some dependencies with:
`sudo apt-get install android-tools-adb android-tools-fastboot fakechroot fakeroot unzip xz-utils wget python python-empy -y`
